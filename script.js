var hitCount;

function initiatePage() {
	console.log("Game Started");
	hideButtons();
	rulesPopup();
}

function initiateGame() {
	setPlayerHand();
	setDealerHandRightDefault();
	resetBackground("dW");
	resetBackground("pW");
	showButtons();
	removeHits();
	updateDealer("dScore");
	updateScore("pScore", "playerCard");
	hitCount = 2;
	hideAce();
	checkForAce();
}

function rulesPopup (){
	var modal = document.querySelector(".modal");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
        modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }

    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
}

function getName() {
	var name;
	var person = prompt("Please enter your name: ", "Player");
	if(person == null || person == "" || person.length > 25){
		alert("Invalid Name. Try again!")
		name = "Player";
		var res = name.toUpperCase();
	}
	else {
		name = person;
		var res = name.toUpperCase();
	}
	document.getElementById("pName").setAttribute("value", res);
}

function generateRandomNumber(min, max) {
	var tmp = 0;
	tmp = Math.random()* max + min;
	return Math.floor(tmp);
}

function getCardValue(val) {
	var tmp =  val % 13;
	switch(tmp) {
		case 1:
			result = 11;
			break;
		case 11:
		case 12:
		case 0:
			result = 10;
			break;
		default:
			result = tmp;
			break;
	}
	return result;
}

function checkForAce() {
	var cards = document.getElementsByClassName("playerCard");
	for(var i=0; i<cards.length; i++) {
		var cardVal = cards[i].getAttribute("val");
		if(parseInt(cardVal) == 11) {
			showAce();
		}
		else if(getScore("playerCard") == 21) {
			hideAce();
		}
	}
}

function aceOne(){
	var cards = document.getElementsByClassName("playerCard");
	var score = 0;
	for(var i=0; i<cards.length; i++) {
		var cardVal = cards[i].getAttribute("val");
		if(parseInt(cardVal) == 11) {
			score -= 10;
			hideAce();
			document.getElementById("pCardImg"+i).setAttribute("val", "1");
			update
		}
		score += parseInt(cardVal);
	}
	document.getElementById("pScore").innerHTML = score;
}

function setCardImage(imgID) {
	var rnd = generateRandomNumber(1, 52),
		cardValue = getCardValue(rnd);
	document.getElementById(imgID).src = "img/" + rnd + ".png";
	document.getElementById(imgID).setAttribute("val", cardValue);
}

function setDealerHandRightDefault() {
	setCardImage("dCardImg1");
	document.getElementById("dCardImg2").src = "img/back.png";
}

function setDealerHandRight() {
	setCardImage("dCardImg2");
}

function setDealerHand() {
	setCardImage("dCardImg1");
	setCardImage("dCardImg2");
}

function setPlayerHand() {
	setCardImage("pCardImg1");
	setCardImage("pCardImg2");
}

function updateScore(target, source) {
	var cards = document.getElementsByClassName(source);
	var score = 0;
	for(i=0; i<cards.length; i++) {
		var cardVal = cards[i].getAttribute("val");
		score += parseInt(cardVal);
	}
	document.getElementById(target).innerHTML = score;
	if(getScore("playerCard") == 21){
		setDealerHandRight();
		updateDealerScore();
		winner();
	}
}

function updateDealer(target) {
	var card = document.getElementById("dCardImg1");
	var cardVal = card.getAttribute("val");
	var score = parseInt(cardVal);
	
	document.getElementById(target).innerHTML = score;
}

function updateDealerScore() {
	var cards = document.getElementsByClassName("dealerCard");
	var score = 0;
	for(i=0; i<cards.length; i++) {
		var cardVal = cards[i].getAttribute("val");
		score += parseInt(cardVal);
	}
	document.getElementById("dScore").innerHTML = score;
}

function getScore(source) {
	var cards = document.getElementsByClassName(source);
	var score = 0;
	for(i=0; i<cards.length; i++) {
		var cardVal = cards[i].getAttribute("val");
		score += parseInt(cardVal);
	}
	return score;
}

function winner() {
	var playerScore = getScore("playerCard");
	var dealerScore = getScore("dealerCard");
	var winningNum = 21;
	var winnerSound = new Audio('sounds/Winner.mp3');
	var loserSound = new Audio('sounds/Loser.mp3');
	var tieSound = new Audio('sounds/Tie.mp3');

	if (playerScore == dealerScore){
		console.log("Tie");
		tieBackground("pW");
		tieBackground("dW");
		hideButtons();
		tieSound.play();
	}
	else if (playerScore > dealerScore && playerScore <= winningNum) {
		console.log("The Player Wins");
		winnerBackground("pW");
		resetBackground("dW");
		hideButtons();
		winnerSound.play();
	}
	else if (playerScore < dealerScore || playerScore >= winningNum) {
		console.log("The Dealer Wins");
		winnerBackground("dW");
		resetBackground("pW");
		hideButtons();
		//loserSound.play();
	}
	else {
		console.log("Error");
	}
	
}

function winnerBackground(winnerID) {
	document.getElementById(winnerID).style.background = "linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(26,196,24,1) 50%, rgba(255,255,255,1) 100%)";
}

function resetBackground(winnerID) {
	document.getElementById(winnerID).style.background = "white";
}

function tieBackground(winnerID) {
	document.getElementById(winnerID).style.background = "linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(253,255,0,1) 50%, rgba(255,255,255,1) 100%)";
}

function hit(){
	if(hitCount<5) {
		hitCount++;
		var card = document.createElement("img");
		card.id = "pCardImg" + hitCount;
		card.src = "img/back.png";
		card.className="cardSize playerCard"
		var element = document.getElementById("newCard");
		element.appendChild(card);
		setCardImage("pCardImg"+hitCount);
		checkForAce();
		updateScore("pScore", "playerCard");
		if(getScore("playerCard") >= 21 && hitCount <= 5) {
			setDealerHandRight();
			updateScore("dScore", "dealerCard");
			winner();
			hideButtons();
		}
	}
}

function removeHits() {
	for(i = 3; i<=10; i++) {
		var tmp = document.getElementById("pCardImg"+i);
		if(tmp != null) {
			tmp.parentNode.removeChild(tmp);
		}
	}
}

function stand() {
	setDealerHandRight();
	updateScore("dScore", "dealerCard");
	winner();
	hideButtons();
}

function hideHit() {
	var x = document.getElementById("hit");
    x.style.display = "none";
}

function showHit() {
	var x = document.getElementById("hit");
    x.style.display = "inline-block";
}

function hideStand() {
	var x = document.getElementById("stand");
    x.style.display = "none";
}

function showStand() {
	var x = document.getElementById("stand");
    x.style.display = "inline-block";
}

function hideAce() {
	var x = document.getElementById("aceOne");
    x.style.display = "none";
	
	var z = document.getElementById("aceEleven");
    z.style.display = "none";
}

function showAce() {
	var x = document.getElementById("aceOne");
    x.style.display = "inline-block";
	
	var z = document.getElementById("aceEleven");
    z.style.display = "inline-block";
}

function hideButtons() {
	hideHit();
	hideStand();
	hideAce();
}

function showButtons() {
	showHit();
	showStand();
}


